#include "helper.h"

/* The function which requires authentication. Called after authentication is 
 * successful.
 */

ActionReply FunHelper::read(const QVariantMap &args){
    ActionReply reply;
    QString filename = args["path"].toString();
    QFile file(filename);

    if(file.open(QIODevice::ReadOnly)){
        QTextStream outs(&file);
        QString content;
        content = outs.readAll();
        QVariantMap map;
        map["content"] = QVariant(content);
        reply.setData(map);
    }else{
        qDebug() << "Error" ;
        reply = ActionReply::HelperErrorReply();
        reply.setErrorDescription(file.errorString());
    }

    return reply;
}

KAUTH_HELPER_MAIN("org.kde.have.fun", FunHelper)


