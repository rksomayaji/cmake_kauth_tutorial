#ifndef HELP_H
#define HELP_H

#include <QDir>
#include <QFile>
#include <KAuth>
#include <QLabel>
#include <QString>
#include <QVBoxLayout>
#include <QPushButton>
#include <QMainWindow>

using namespace KAuth;

namespace Ui {
class Help;
}

class Help : public QMainWindow
{
    Q_OBJECT

public:
    explicit Help(QWidget *parent = 0);
    ~Help();
public slots:
    void change_text();
private:
    Ui::Help *ui;
    QLabel *fun_text;
    QPushButton *display_textButton;
    QVBoxLayout *main_layout;
};

#endif
