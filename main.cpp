#include <iostream>
#include "help.h"
#include <QApplication>

using namespace std;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Help h;
    h.show();
    return a.exec();
}
