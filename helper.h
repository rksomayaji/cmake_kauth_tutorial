#ifndef HELPER_H
#define HELPER_H

#include <QFile>
#include <QTextStream>
#include <QDebug>

#include <KAuth>

using namespace KAuth;

class FunHelper : public QObject
{
    Q_OBJECT
public Q_SLOTS:
    ActionReply read(const QVariantMap& args);
};

#include "helper.moc"

#endif //HELPER_H 
