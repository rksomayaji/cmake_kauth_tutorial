#include "help.h"
#include "ui_help.h"
#include <QTextStream>
#include <QDebug>

void Help::change_text()
{
    // The path is a file which is owned by root with r--|---|--- permissions
    QString path = "/home/drsushanth/fun";

    QVariantMap args;
    args["path"] = path;
    
/* Call the Helper action read, attached to the helper id with the file path 
 * as argument
 */
    Action read_action("org.kde.have.fun.read");
    read_action.setHelperId("org.kde.have.fun");
    read_action.setArguments(args);

    // Execute the job requiring the authentication
    ExecuteJob *job = read_action.execute();

/* Evaluate if the person calling is authenticated and display the data. If not
 * authenticated show error
 */
    if(job->exec()){
        QVariantMap map = job->data();
        QString contents = map["content"].toString();
        fun_text->setText(contents);
    }else {
        fun_text->setText("Error code: " + QString::number(job->error()));
    }
}

Help::Help(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Help)
{
    ui->setupUi(this);

    display_textButton = new QPushButton("Load File");
    connect(display_textButton, SIGNAL(clicked(bool)), this,
            SLOT(change_text()));

    fun_text = new QLabel("This is a test.");

    main_layout = ui->mainLayout;
    main_layout->addWidget(fun_text);
    main_layout->addWidget(display_textButton);
}

Help::~Help()
{
    delete ui;
}
